-- LUALOCALS < ---------------------------------------------------------
local ch_colours, ch_draconis, ch_fireworks, ch_flashscreen,
      ch_ion_cannon, ch_player_api, ipairs, math, minetest, pairs,
      string, table, tonumber, vector
    = ch_colours, ch_draconis, ch_fireworks, ch_flashscreen,
      ch_ion_cannon, ch_player_api, ipairs, math, minetest, pairs,
      string, table, tonumber, vector
local math_floor, string_format, string_sub, table_concat, table_remove
    = math.floor, string.format, string.sub, table.concat, table.remove
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local priv_all = modname .. "_all"
minetest.register_privilege(priv_all, {
		description = "Access to ALL " .. modname .. " commands",
		give_to_admin = false,
		give_to_singleplayer = false
	})
local function register_cheat(cmd, desc, params, func)
	local priv_cmd = modname .. "_" .. cmd
	minetest.register_privilege(priv_cmd, {
			description = string_format("Access to the %q %s command", cmd, modname),
			give_to_admin = false,
			give_to_singleplayer = false
		})
	return minetest.register_chatcommand(
		minetest.registered_chatcommands[cmd] and (modname .. "_" .. cmd) or cmd,
		{
			description = desc,
			params = params,
			func = function(name, param)
				local privs = minetest.get_player_privs(name)
				if not (privs[priv_all] or privs[priv_cmd]) then
					return false, string_format("Missing either %q or %q priv",
						priv_cmd, priv_all)
				end
				local parts = param:split(" ")
				if parts[1] and string_sub(parts[1], 1, 1) == "@" then
					name = string_sub(parts[1], 2)
					table_remove(parts, 1)
					param = table_concat(parts, " ")
				end
				local player = minetest.get_player_by_name(name)
				if not player then return false, "invalid player" end
				local pos = player:get_pos()
				if not pos then return false, "invalid pos" end
				return func(name, param, player, pos)
			end
		})
end
local function numchk(param, min, max, granularity)
	local num = tonumber(param)
	if not num then return nil, "invalid number" end
	if min and num < min then return nil, "cannot be < " .. min end
	if granularity then
		num = math_floor((num - (min or 0)) / granularity)
		* granularity + (min or 0)
	end
	if max and num > max then return nil, "cannot be > " .. max end
	return num
end

------------------------------------------------------------------------
-- BINDINGS

minetest.register_chatcommand(
	minetest.registered_chatcommands.bind and (modname .. "_bind") or "bind", {
		description = "Bind a command to sneak+aux1 for current color",
		params = "<command> [param [param] ...]",
		func = function(name, param)
			local player = minetest.get_player_by_name(name)
			if not player then return false, "invalid player" end
			local meta = player:get_meta()
			local color = meta:get_int("colour")
			if not color then return "invalid color" end
			player:get_meta():set_string(modname .. "_" .. color, param)
			return true, param == ""
			and string_format("unbound %d", color)
			or string_format("bound %d to %q", color, param)
		end
	})
do
	local bindwatch = {}
	local function trigger(player, pname)
		local meta = player:get_meta()
		local color = meta:get_int("colour")
		if not color then return end
		local cmd = player:get_meta():get_string(modname .. "_" .. color)
		if not (cmd and cmd ~= "") then return end
		local parts = cmd:split(" ")
		cmd = minetest.registered_chatcommands[parts[1]]
		if not cmd then return end
		table_remove(parts, 1)
		local _, msg = cmd.func(pname, table_concat(parts, " "))
		if msg then return minetest.chat_send_player(pname, msg) end
	end
	local function checkplayer(player)
		local pname = player:get_player_name()
		local control = player:get_player_control()
		local trig = control.sneak and control.aux1
		if trig then
			if bindwatch[pname] then return end
			bindwatch[pname] = true
			trigger(player, pname)
		else
			bindwatch[pname] = nil
		end
	end
	minetest.register_globalstep(function()
			for _, player in ipairs(minetest.get_connected_players()) do
				checkplayer(player)
			end
		end)
end

------------------------------------------------------------------------
-- CHANGE PLAYER STATUS

register_cheat(
	"colors",
	"Replace player's list of available colours",
	"[color name or id] [[color name or id] ...]",
	function(_, param, player)
		local ct = {}
		if param == "all" then
			for k in pairs(ch_colours.by_name) do
				ct[k] = true
			end
		else
			for _, k in ipairs(param:split(" ")) do
				ct[tonumber(k) and ch_colours.by_num[tonumber(k)] or k] = true
			end
		end
		ch_player_api.set_colours(player, ct)
	end)
minetest.registered_chatcommands.colours = minetest.registered_chatcommands.colors

------------------------------------------------------------------------
-- ADD/REMOVE NODES

register_cheat(
	"pn",
	"Instantly place a node at player's feet",
	nil,
	function(_, _, player, pos)
		minetest.set_node(pos, {name = "world:black"})
		local vel = player:get_velocity()
		if vel.y <= 0 then
			player:add_velocity({x = 0, y = 6.5, z = 0})
		end
	end)

register_cheat(
	"zap",
	"Instantly destroy node player is pointing at",
	nil,
	function(_, _, player)
		local start = player:get_pos()
		start.y = start.y + player:get_properties().eye_height
		local target = vector.add(start, vector.multiply(player:get_look_dir(), 4))
		for pt in minetest.raycast(start, target, false, false) do
			if pt.type == "node" then
				local node = {name = "world:yellowb", param = 0, param2 = 0}
				minetest.set_node(pt.under, node)
				return ch_colours.trigger(node, pt.under,
					{x = 0, y = 1, z = 0},
					player:get_player_name())
			end
		end
	end)

------------------------------------------------------------------------
-- WEAPONS

register_cheat(
	"lo",
	"Fire a low-altitude firework from player's location",
	nil,
	function(_, _, _, pos)
		ch_fireworks.launch(pos, "yellow", false, false)
	end)
register_cheat("hi",
	"Fire a high-altitude firework from player's location",
	nil,
	function(_, _, _, pos)
		ch_fireworks.launch(pos, "red", true, false)
	end)
register_cheat(
	"ap",
	"Fire a shield-breaking firework from player's location",
	nil,
	function(_, _, _, pos)
		ch_fireworks.launch(pos, "blue", false, true)
	end)
register_cheat(
	"ion",
	"Fire an ion cannon beam firework from player's location",
	nil,
	function(_, _, _, pos)
		ch_ion_cannon.fire(pos)
	end)

------------------------------------------------------------------------
-- SUMMON DRAGONS

register_cheat(
	"summon",
	"Summon dragon of the specified color at the player's location",
	"<dragon color>",
	function(_, param, _, pos)
		local entname = "ch_draconis:" .. param .. "_dragon"
		if not minetest.registered_entities[entname] then
			return false, string_format("Entity %q not found", entname)
		end
		ch_draconis.spawn_dragon(pos, entname)
	end)

------------------------------------------------------------------------
-- TELEPORTATION

register_cheat(
	"royal",
	"Teleport to royal room",
	nil,
	function(_, _, _, pos)
		local abber = minetest.registered_nodes["ch_silver:abberation"]
		if not abber then return false, "teleportation node def not found" end
		local oldy = pos.y
		pos.y = -3136
		local oldset = minetest.set_node
		function minetest.set_node() end
		local oldmeta = minetest.get_meta
		function minetest.get_meta(...)
			pos.y = oldy
			minetest.set_node = oldset
			minetest.get_meta = oldmeta
			return oldmeta(...)
		end
		abber.on_timer(pos, 0)
		minetest.set_node = oldset
		minetest.get_meta = oldmeta
	end
)

do
	local quad_size = 9 * 16
	local quad_y = -3015
	register_cheat(
		"storage",
		"Teleport to storage room",
		nil,
		function(_, _, player, pos)
			pos = {
				x = math_floor(pos.x / quad_size) * quad_size + 7,
				y = quad_y + 2,
				z = math_floor(pos.z / quad_size) * quad_size + 7
			}
			ch_flashscreen.showflash(player, "#000099", 3)
			player:set_pos(pos)
			minetest.sound_play("blue_normal", {
					pos = pos,
					gain = 1.0,
					loop = false
				})
		end
	)
end

------------------------------------------------------------------------
-- CONVERT BUILDINGS

register_cheat(
	"unbuild",
	"Convert building back to ordinary nodes in specified radius around player",
	"<radius>",
	function(_, param, _, pos)
		local num, err = numchk(param, 0, 128, 1)
		if err then return false, err end
		for z = pos.z - num, pos.z + num do
			for y = pos.y - num, pos.y + num do
				for x = pos.x - num, pos.x + num do
					local np = {x = x, y = y, z = z}
					local old = minetest.get_node(np).name
					local new = old:gsub("buildings:", "world:")
					if old ~= new and minetest.registered_nodes[new] then
						minetest.swap_node(np, {name = new})
					end
				end
			end
		end
	end)

------------------------------------------------------------------------
-- ZOOM FOV

register_cheat(
	"fov",
	"Enable and set zoom FOV for player",
	"<fov angle degrees>",
	function(_, param, player)
		local fov, err = numchk(param, 0, 180)
		if not fov then return false, err end
		player:set_properties({zoom_fov = fov})
	end)
