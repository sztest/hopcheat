This mod adds a lot of new "cheat" chat commands for ColourHop, which may especially be useful for development/testing/debugging or creating demo/trailer videos.

- For convenience, you can also bind an arbitrary command to the Sneak+Aux1 hotkey for each color.  This is useful for things like the place/zap node commands, or firing weapons, which you are likely to want to do repeatedly without opening the chat interface each time.
- Each command has an individual privilege so you can grant access per-player.  There is also an "all" privilege for convenience.
- Each command can be run targeting another player, in which case it will affect that player as if they were the one to run the command (though using *your* privilege for the check).  This allows you to e.g. administer which colors a player has access to, to allow them to join a late-game party without having to make everyone wait while they beat the early bosses.

Added commands include:

- `/bind` to bind a command to the hotkey for your currently-selected color.
- `/colors` (or `/colours`) to set which colors you have access to.
- `/pn` to place a new node as if you had just hopped red (but without having to stand on red or do the rigamarole of using a "crane").
- `/zap` to destroy the node you are pointing at (works on walls too).
- `/lo`, `/hi`, `/ap` and `/ion` to fire various surface-to-air weapons.
- `/summon` to summon a dragon as if an altar existed where your feet are.
- `/unbuild` to convert an "activated" or "spent" building back into its original nodes, so it can be triggered again.
- `/fov` to grant a player the ability to zoom in.
- `/lag` to introduce artificial lag to the server step, useful for testing robustness of mechanics under non-ideal server loading effects.

See the in-game `/help` for a full list of commands.